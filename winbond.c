#include "winbond.h"

//*****************************************************************************
//
// The CPU usage in percent, in 16.16 fixed point format.
//
//*****************************************************************************
static uint32_t g_ui32CPUUsage = 0;

//*****************************************************************************
//
// The number of seconds elapsed since the start of the program.  This value is
// maintained by the SysTick interrupt handler.
//
//*****************************************************************************
static uint32_t g_ui32Seconds = 0;

volatile uint32_t g_cmd_count;
uint32_t g_rx_dst_inc;
uint32_t g_tx_src_inc;

//*****************************************************************************
//
// The UART initialization for console printing
//
//*****************************************************************************
void init_uart(void) {
    // Enable the GPIO Peripheral used by the UART.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Enable UART0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Configure GPIO Pins for UART mode.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 9600, 16000000);
}

//*****************************************************************************
//
// Helper function that formats 256 bytes of data into an easier to read grid
//
//*****************************************************************************
void print_page_bytes(uint8_t *page_buffer) {
    uint8_t i, j;
    for (i = 0; i < 16; i++) {
        for (j = 0; j < 16; j++) {
            UARTprintf("\%02x ", page_buffer[i * 16 + j]);
        }
        UARTprintf("\n");
    }
}

uint8_t byte_transfer(uint8_t data) {
    SSIDataPut(SSI0_BASE, data);
    while(SSIBusy(SSI0_BASE));
    uint32_t rx_buf;
    SSIDataGet(SSI0_BASE, &rx_buf);
    while(SSIBusy(SSI0_BASE));
    return (uint8_t)rx_buf;
}

//*****************************************************************************
//
// Ping busy bit from Flash status register
//
//*****************************************************************************
//uint8_t chip_busy(void) {
//    uint32_t busy;
//    chip_select(~GPIO_PIN_3);
//    byte_transfer(WB_READ_STATUS_REG_1);
//    busy = byte_transfer(0x00);
//    chip_select(GPIO_PIN_3);
//    return (uint8_t)busy;
//}

void chip_busy(void) {
    chip_select(~GPIO_PIN_3);
    byte_transfer(WB_READ_STATUS_REG_1);
    while (byte_transfer(0x00) & 1);
//    byte_transfer(0x00);
//    byte_transfer(0x00);
//    byte_transfer(0x00);
//    byte_transfer(0x00);
//    byte_transfer(0x00);
//    byte_transfer(0x00);
    chip_select(GPIO_PIN_3);
}

//*****************************************************************************
//
// Read flash device info
//
//*****************************************************************************
void read_id(void) {
//    uint8_t cmd[] = {WB_READ_ID, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint8_t cmd[4];

    UARTprintf("Acquiring Flash Info...\n");

    // Transmitting 4 data and expects two bytes of device id + manufacturer id
    // TX           RX
    // WB_READ_ID   XX
    // 0x00         XX
    // 0x00         XX
    // 0x00         XX
    // Master needs to drive clock to allow man_id and dev_id to be shifted out
    // 0x00         man_id
    // 0x00         dev_id

    cmd[0] = WB_READ_ID;
    cmd[1] = 0x00;
    cmd[2] = 0x00;
    cmd[3] = 0x00;

    // number of command address bytes
    g_cmd_count = 4;

    g_rx_dst_inc = UDMA_DST_INC_8;
    g_tx_src_inc = UDMA_SRC_INC_NONE;

    uint8_t tx_dummy = 0x00;

//    ssi_transfer(cmd, &tx_dummy, 2, UDMA_SRC_INC_NONE, cmd, 2);
    ssi_transfer(cmd, 4, &tx_dummy, cmd, 2);
//    ssi_transfer(cmd, 6, &tx_dummy, 2, cmd, 2);

//    while (chip_busy() & 1);
    chip_busy();

    UARTprintf("Manufacturing ID: \%2x\n", cmd[0]);
    UARTprintf("Device ID: \%2x\n", cmd[1]);
}

//*****************************************************************************
//
// Read flash based on given starting address and number of bytes to read
//
//*****************************************************************************
void my_spi_read(uint32_t addr, uint32_t num_byte, uint8_t *dst) {
//    UARTprintf("Reading \%0d Bytes of Data from Starting Address 0x\%02x",
//               num_byte, (addr >> 16) & 0xff);
//    UARTprintf("\%02x", (addr >> 8) & 0xff);
//    UARTprintf("\%02x\n", addr & 0xff);

    uint8_t cmd[4];

    cmd[0] = WB_READ_DATA;
    cmd[1] = (addr >> 16) & 0xFF;
    cmd[2] = (addr >> 8) & 0xFF;
    cmd[3] = (addr >> 0) & 0xFF;

    g_cmd_count = 4;

    g_rx_dst_inc = UDMA_DST_INC_8;
    g_tx_src_inc = UDMA_SRC_INC_NONE;

    uint8_t tx_dummy = 0x00;

    ssi_transfer(cmd, 4, &tx_dummy, dst, num_byte);

//    while (chip_busy() & 1);
    chip_busy();
    print_page_bytes(dst);
//    UARTprintf("Done\n\n");
}

////*****************************************************************************
////
//// Helper function to my_spi_write
////
////*****************************************************************************
//void write_page(uint32_t addr, uint32_t num_byte, uint8_t *src) {
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//
//    byte_transfer(WB_WRITE_ENABLE);
//
//    chip_select(GPIO_PIN_3);     // /Chip select high
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//
//    byte_transfer(WB_PAGE_PROGRAM);
//    byte_transfer((addr >> 16) & 0xFF);
//    byte_transfer((addr >>  8) & 0xFF);
//    byte_transfer((addr >>  0) & 0xFF);
//
//    int i;
//    for (i = 0; i < num_byte; i++) {
//        byte_transfer(src[i]);
//    }
//
//    chip_select(GPIO_PIN_3);     // /Chip select high
//    not_busy();
//}

////*****************************************************************************
////
//// Write flash base on given starting address, number of bytes and data
////
////*****************************************************************************
//void my_spi_write(uint32_t addr, uint32_t num_byte, uint8_t *src) {
////    printf("Writing %n Bytes of Data from Starting Address 0x%2",
////           num_byte, (addr >> 16));
////    printf("%2", (addr >> 8));
////    printf("%2\r\n\r\n", addr);
//
//    uint8_t num_page = 0, num_single = 0, not_page_aligned = 0, count = 0;
//
//    // check for page alignment
//    not_page_aligned = addr % WB_PAGE_SIZE;
//
//    // count of free bytes left between starting address and next closest
//    // page bound(upper bound)
//    count = WB_PAGE_SIZE - not_page_aligned;
//
//    // number of full pages
//    num_page = num_byte / WB_PAGE_SIZE;
//
//    // number of single bytes remaining (too small to form a page, <256B)
//    num_single = num_byte % WB_PAGE_SIZE;
//
//    // Page is aligned
//    if (not_page_aligned == 0) {
//        while (num_page--) {
//            write_page(addr, WB_PAGE_SIZE, src);
//            addr += WB_PAGE_SIZE;
//            src += WB_PAGE_SIZE;
//        }
//
//        // if total page is not a whole number,
//        // this takes care of the remainings
//        if (num_single > 0) {
//            write_page(addr, num_single, src);
//        }
//        // }
//    // page not aligned
//    } else {
//        // if the count of free bytes between starting addr and next closest
//        // page bound is too small
//        if (num_byte > count) {
//            // write the head(excess data bytes) off to make the rest page
//            // aligned
//            write_page(addr, count, src);
//
//            // updating address and source buffer index
//            addr += count;
//            src += count;
//
//            // resetting data byte information
//            num_byte -= count;
//            num_page = num_byte / WB_PAGE_SIZE;
//            num_single = num_byte % WB_PAGE_SIZE;
//
//            while (num_page--) {
//                write_page(addr, WB_PAGE_SIZE, src);
//                addr += WB_PAGE_SIZE;
//                src += WB_PAGE_SIZE;
//            }
//
//            if (num_single > 0) {
//                write_page(addr, num_single, src);
//            }
//
//        } else {
//            write_page(addr, num_byte, src);
//        }
//    }
//
////    printf("Done\r\n\r\n");
////    printf("Wrote %n Bytes of Data from Starting Address 0x%x",
////           num_byte, (addr >> 16));
////    printf("%x", (addr >> 8));
////    printf("%x\r\n\r\n", addr);
//}

////*****************************************************************************
////
//// Helper function for my_spi_erase
////
////*****************************************************************************
//void block_erase_64KB(uint32_t addr) {
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//
//    byte_transfer(WB_WRITE_ENABLE);
//
//    chip_select(GPIO_PIN_3);     // /Chip select highs
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//    byte_transfer(WB_BLOCK_ERASE_64);
//
//    byte_transfer((addr >> 16) & 0xFF);
//    byte_transfer((addr >>  8) & 0xFF);
//    byte_transfer((addr >>  0) & 0xFF);
//
//    chip_select(GPIO_PIN_3);     // /Chip select high
//
//    // Check busy bit
//    not_busy();
//}

////*****************************************************************************
////
//// Erase a block(64kB) given starting address
////
////*****************************************************************************
//void my_spi_erase(uint32_t addr, uint32_t num_byte) {
////    printf("Erasing %n Bytes of Data from Starting Address 0x%2",
////           num_byte, (addr >> 16));
////    printf("%2", (addr >> 8));
////    printf("%2\r\n", addr);
//
//    uint8_t num_block = 0, num_single = 0;
//
//    num_block = num_byte / WB_BLOCK_SIZE_64;  // number of full blocks
//    num_single = num_byte % WB_BLOCK_SIZE_64;  // number of leftovers
//
//    while (num_block--) {
//        block_erase_64KB(addr);
//        addr += WB_BLOCK_SIZE_64;
//        num_byte -= WB_BLOCK_SIZE_64;
//    }
//
//    if (num_single > 0) {
//        block_erase_64KB(addr);
//    }
//
////    printf("Done\r\n\r\n");
////    printf("Erased %n Bytes of Data from Starting Address 0x%x",
////           num_byte, (addr >> 16));
////    printf("%x", (addr >> 8));
////    printf("%x\r\n", addr);
//}

////*****************************************************************************
////
//// Erase the whole chip
////
////*****************************************************************************
//void chip_erase(void) {
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//    byte_transfer(WB_WRITE_ENABLE);
//    chip_select(GPIO_PIN_3);     // /Chip select high
//    chip_select(~GPIO_PIN_3);    // /Chip select low
//    byte_transfer(WB_CHIP_ERASE);
//    chip_select(GPIO_PIN_3);     // /Chip select high
//    UARTprintf("Erasing Chip ...\n");
//    not_busy();
//    UARTprintf("Chip has been successfully erased!\n\n");
//}

//*****************************************************************************
//
// Main
//
//*****************************************************************************
int main(void) {
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 |
                   SYSCTL_USE_PLL |
                   SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_16MHZ);

    // Initialize peripherals
    init_uart();
    init_spi();
    init_udma();

    //
    // Configure SysTick to occur 100 times per second, to use as a time
    // reference.  Enable SysTick to generate interrupts.
    //
//    SysTickPeriodSet(SysCtlClockGet() / SYSTICKS_PER_SECOND);
//    SysTickIntEnable();
//    SysTickEnable();

    //
    // Initialize the CPU usage measurement routine.
    //
//    CPUUsageInit(SysCtlClockGet(), SYSTICKS_PER_SECOND, 2);

    //
    // Show statistics headings.
    //
//    UARTprintf("CPU    \n");
//    UARTprintf("Usage  \n");

//    while (chip_busy() & 1);
//    chip_busy();
//    read_id();

//    chip_erase();

//    block_erase_64KB(0x3F0000);
    uint8_t buf[256];
    my_spi_read(0x000100, 256, buf);
//    my_spi_read(0x3FFF00, 256, buf);
//    uint8_t write_data[] = {0x12, 0x02, 0x03, 0x04};
//    write_page(0x3FFF00, 4, write_data);
//    my_spi_read(0x3FFF00, 256, buf);

//    static uint32_t ui32PrevSeconds;
//
//    //
//    // Remember the current SysTick seconds count.
//    //
//    ui32PrevSeconds = g_ui32Seconds;
//
//    //
//    // Check out CPU usage
//    // -----------------------------------------------------------------------
//    while (1) {
//        if (g_ui32Seconds != ui32PrevSeconds) {
//
//            //
//            // Print a message to the display showing the CPU usage percent.
//            // The fractional part of the percent value is ignored.
//            //
//            UARTprintf("\r%3d%%   \n", g_ui32CPUUsage >> 16);
//
//            //
//            // Remember the new seconds count.
//            //
//            ui32PrevSeconds = g_ui32Seconds;
//
//        }
//
//        //
//        // See if we have run int32_t enough and exit the loop if so.
//        //
//        if(g_ui32Seconds >= 10)
//        {
//            break;
//        }
//    }
//    //-----------------------------------------------------------------------

    return 0;
}

//*****************************************************************************
//
// The interrupt handler for the SysTick timer.  This handler will increment a
// seconds counter whenever the appropriate number of ticks has occurred.  It
// will also call the CPU usage tick function to find the CPU usage percent.
//
//*****************************************************************************
void SysTickHandler(void) {
    static uint32_t ui32TickCount = 0;

    //
    // Increment the tick counter.
    //
    ui32TickCount++;

    //
    // If the number of ticks per second has occurred, then increment the
    // seconds counter.
    //
    if(!(ui32TickCount % SYSTICKS_PER_SECOND))
    {
        g_ui32Seconds++;
    }

    //
    // Call the CPU usage tick function.  This function will compute the amount
    // of cycles used by the CPU since the last call and return the result in
    // percent in fixed point 16.16 format.
    //
    g_ui32CPUUsage = CPUUsageTick();
}
