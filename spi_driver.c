#include "spi_driver.h"

//*****************************************************************************
//
// The SSI0 Buffers
//
//*****************************************************************************
static uint8_t *g_ui8SSITxBuf;
static uint8_t *g_ui8SSIRxBuf;
//static uint8_t *g_ui8SSITxCmdBuf;
//static uint8_t *g_ui8SSIRxCmdBuf;

//*****************************************************************************
//
// The count of uDMA errors.  This value is incremented by the uDMA error
// handler.
//
//*****************************************************************************
static uint32_t g_ui32uDMAErrCount = 0;

//*****************************************************************************
//
// The count of SSI0 buffers filled
//
//*****************************************************************************
static uint32_t g_ui32SSIDataCount = 0;

//*****************************************************************************
//
// Flag to determine if command / address bytes has been sent
//
//*****************************************************************************
static uint32_t g_cmd_sent = 0;

//*****************************************************************************
//
// The control table used by the uDMA controller.  This table must be aligned
// to a 1024 byte boundary.
//
//*****************************************************************************
#pragma DATA_ALIGN(pui8ControlTable, 1024)
uint8_t pui8ControlTable[1024];

//*****************************************************************************
//
// The interrupt handler for uDMA errors.  This interrupt will occur if the
// uDMA encounters a bus error while trying to perform a transfer.  This
// handler just increments a counter if an error occurs.
//
//*****************************************************************************
void uDMAErrorHandler(void) {
    uint32_t ui32Status;

    //
    // Check for uDMA error bit
    //
    ui32Status = uDMAErrorStatusGet();

    //
    // If there is a uDMA error, then clear the error and increment
    // the error counter.
    //
    if(ui32Status) {
        uDMAErrorStatusClear();
        g_ui32uDMAErrCount++;
    }
}

//*****************************************************************************
//
// Initialize uDMA
//
//*****************************************************************************
void init_udma(void) {
    //
    // Enable the uDMA controller at the system level.  Enable it to continue
    // to run while the processor is in sleep.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_UDMA);

    //
    // Enable the uDMA controller error interrupt.  This interrupt will occur
    // if there is a bus error during a transfer.
    //
    IntEnable(INT_UDMAERR);

    //
    // Enable the uDMA controller.
    //
    uDMAEnable();

    //
    // Point at the control table to use for channel control structures.
    //
    uDMAControlBaseSet(pui8ControlTable);
}

//*****************************************************************************
//
// Initialize SPI
//
//*****************************************************************************
void init_spi(void) {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_SSI0);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI0));

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA));

    // SSI pin configuration
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA4_SSI0RX);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE,
                   GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_2);

    // Configure SSI operation mode
    SSIConfigSetExpClk(SSI0_BASE,
                       SysCtlClockGet(),
                       SSI_FRF_MOTO_MODE_0,
                       SSI_MODE_MASTER,
                       1000000,
                       8);

    // SSI CS pin configuration
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_PIN_3);

    //--------------------------------------------------------
    // Toggle this pin when interrupt happens
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB));

    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, ~GPIO_PIN_2);

    //--------------------------------------------------------

    // Enable SSI interface (post configuration)
    SSIEnable(SSI0_BASE);

    uint32_t garbage = 0;
    // Read any residual data from the SSI port.
    while (SSIDataGetNonBlocking(SSI0_BASE, &garbage));

    // Enable the SSI interrupt.
    IntEnable(INT_SSI0);

    // Enable processor interrupts.
    IntMasterEnable();
}

//*****************************************************************************
//
// Initiate communication with flash by transferring command / address bytes
//
//*****************************************************************************
void ssi_transfer(uint8_t *cmd_buf, uint32_t cmd_count, uint8_t *tx_buf,
                  uint8_t *rx_buf, uint32_t data_count) {

//    if (data_count > DMA_MAX_TRANSFER_COUNT) {
//
//    }
//
//    g_ui8SSITxBuf = tx_buf;
//    g_ui8SSIRxBuf = rx_buf;

//    g_ui32SSIDataCount = data_count;

    //
    // Enable the uDMA interface for both TX and RX channels.
    //
    SSIDMAEnable(SSI0_BASE, SSI_DMA_RX | SSI_DMA_TX);

    //*************************************************************************
    //uDMA SSI0 RX
    //*************************************************************************

    //
    // Put the attributes in a known state for the uDMA SSI0RX channel.  These
    // should already be disabled by default.
    //
    uDMAChannelAttributeDisable(UDMA_CHANNEL_SSI0RX,
                                UDMA_ATTR_USEBURST |
                                UDMA_ATTR_ALTSELECT |
                                (UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK));

    //
    // Configure the control parameters for the primary control structure for
    // the SSIORX channel.
    //
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_NONE |
                          UDMA_DST_INC_8 | UDMA_ARB_4);

    //
    // Configure the control parameters for the alternate control structure for
    // the SSIORX channel.
    //
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0RX | UDMA_ALT_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_NONE |
                          g_rx_dst_inc | UDMA_ARB_4);

    //
    // Set up the transfer parameters for the SSI0RX Channel (primary)
    //
    uDMAChannelTransferSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
                           UDMA_MODE_PINGPONG,
                           (void *)(SSI0_BASE + SSI_O_DR),
                           cmd_buf,
                           cmd_count);

    //
    // Set up the transfer parameters for the SSI0RX Channel (alternate)
    //
    uDMAChannelTransferSet(UDMA_CHANNEL_SSI0RX | UDMA_ALT_SELECT,
                           UDMA_MODE_PINGPONG,
                           (void *)(SSI0_BASE + SSI_O_DR),
                           rx_buf,
                           data_count);


    //*************************************************************************
    //uDMA SSI0 TX
    //*************************************************************************

    //
    // Put the attributes in a known state for the uDMA SSI0TX channel.  These
    // should already be disabled by default.
    //
    uDMAChannelAttributeDisable(UDMA_CHANNEL_SSI0TX,
                                UDMA_ATTR_ALTSELECT |
                                UDMA_ATTR_HIGH_PRIORITY |
                                UDMA_ATTR_REQMASK);

    //
    // Configure the control parameters for the SSI0 TX.
    //
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 | UDMA_SRC_INC_8 |
                          UDMA_DST_INC_NONE | UDMA_ARB_4);

    //
    // Configure the control parameters for the SSI0 TX.
    //
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0TX | UDMA_ALT_SELECT,
                          UDMA_SIZE_8 | g_tx_src_inc |
                          UDMA_DST_INC_NONE | UDMA_ARB_4);

    //
    // Set up the transfer parameters for the uDMA SSI0 TX channel.  This will
    // configure the transfer source and destination and the transfer size.
    // Basic mode is used because the peripheral is making the uDMA transfer
    // request.  The source is the TX buffer and the destination is the SSI0
    // data register.
    //
    uDMAChannelTransferSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
                           UDMA_MODE_PINGPONG,
                           cmd_buf,
                           (void *)(SSI0_BASE + SSI_O_DR),
                           cmd_count);

    uDMAChannelTransferSet(UDMA_CHANNEL_SSI0TX | UDMA_ALT_SELECT,
                           UDMA_MODE_PINGPONG,
                           tx_buf,
                           (void *)(SSI0_BASE + SSI_O_DR),
                           data_count);

    // Send command / address bytes and pop out return bytes
    chip_select(~GPIO_PIN_3);

    //
    // Now both the uDMA SSI0 TX and RX channels are primed to start a
    // transfer.  As soon as the channels are enabled, the peripheral will
    // issue a transfer request and the data transfers will begin.
    //
    uDMAChannelEnable(UDMA_CHANNEL_SSI0RX);
    uDMAChannelEnable(UDMA_CHANNEL_SSI0TX);

    // Wait until the SSI chip select de-asserts, indicating the end of the
    // transfer.
    while (!(GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_3) & GPIO_PIN_3));
}

//*****************************************************************************
//
// Chip select
//
//*****************************************************************************
void chip_select(uint8_t val) {
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, val);
}

//*****************************************************************************
//
// SSI ISR
//
//*****************************************************************************
void SSI0IntHandler(void) {
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_2); // indicate ISR entry

    uint32_t ui32Status;
    uint32_t ui32Mode;

    ui32Status = SSIIntStatus(SSI0_BASE, 1);

    SSIIntClear(SSI0_BASE, ui32Status);

    ui32Mode = uDMAChannelModeGet(UDMA_CHANNEL_SSI0RX | UDMA_ALT_SELECT);

    // Data transfer done, pull CS high
    if (ui32Mode == UDMA_MODE_STOP) {
        IntDisable(INT_UDMAERR);
        IntDisable(INT_SSI0);
        chip_select(GPIO_PIN_3);
    }

    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, ~GPIO_PIN_2); // indicate ISR entry
}

//void SSI0IntHandler(void) {
//    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_2); // indicate ISR entry
//    uint32_t ui32Status;
//    uint32_t ui32Mode;
//
//    ui32Status = SSIIntStatus(SSI0_BASE, 1);
//
//    SSIIntClear(SSI0_BASE, ui32Status);
//
//    if (g_cmd_sent) {
//        chip_select(GPIO_PIN_3);
//
//    } else {
//        // Enters here meaning command / address bytes were sent
//        // Setup another transfer for the data that we're interested
//
//        ui32Mode = uDMAChannelModeGet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT);
//
//        // RX DMA complete
//        // If it's a write, g_rx_dst_inc will be UDMA_DST_INC_NONE
//        // if read, g_rx_dst_inc = UDMA_DST_INC_8
//        if (ui32Mode == UDMA_MODE_STOP) {
//            g_cmd_sent = 1; // signal that cmd and addr were transferred so next
//                            // ISR entry will pull SS line HIGH
//
//            uDMAChannelControlSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
//                                  UDMA_SIZE_8 | UDMA_SRC_INC_NONE |
//                                  g_rx_dst_inc | UDMA_ARB_4);
//
//            uDMAChannelTransferSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
//                                   UDMA_MODE_BASIC,
//                                   (void *)(SSI0_BASE + SSI_O_DR),
//                                   g_ui8SSIRxBuf,
//                                   g_ui32SSIRxCount);
//
//            uDMAChannelEnable(UDMA_CHANNEL_SSI0RX);
//        }
//
//        // TX DMA complete
//        // If it's a write, g_tx_dst_inc will be UDMA_SRC_INC_8
//        // if read, g_tx_src_inc = UDMA_SRC_INC_NONE
//        if (!uDMAChannelIsEnabled(UDMA_CHANNEL_SSI0TX)) {
//            uDMAChannelControlSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
//                                  UDMA_SIZE_8 | g_tx_src_inc |
//                                  UDMA_DST_INC_NONE | UDMA_ARB_4);
//
//            uDMAChannelTransferSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
//                                   UDMA_MODE_BASIC,
//                                   g_ui8SSITxBuf,
//                                   (void *)(SSI0_BASE + SSI_O_DR),
//                                   g_ui32SSITxCount);
//
//            uDMAChannelEnable(UDMA_CHANNEL_SSI0TX);
//        }
//    }
//
//    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, ~GPIO_PIN_2); // indicate ISR exit
//}

//void SSI0IntHandler(void) {
//    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_2); // indicate ISR entry
//    uint32_t ui32Status;
//    uint32_t ui32Mode;
//
//    ui32Status = SSIIntStatus(SSI0_BASE, 1);
//
//    SSIIntClear(SSI0_BASE, ui32Status);
//
//    if (g_cmd_sent) {
//        chip_select(GPIO_PIN_3);
//
//    } else {
//        // Enters here meaning command / address bytes were sent
//        // Setup another transfer for the data that we're interested
//
//        ui32Mode = uDMAChannelModeGet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT);
//
//        // RX DMA complete
//        // If it's a write, g_rx_dst_inc will be UDMA_DST_INC_NONE
//        // if read, g_rx_dst_inc = UDMA_DST_INC_8
//        if (ui32Mode == UDMA_MODE_STOP) {
//            g_cmd_sent = 1; // signal that cmd and addr were transferred so next
//                            // ISR entry will pull SS line HIGH
//
//            uDMAChannelControlSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
//                                  UDMA_SIZE_8 | UDMA_SRC_INC_NONE |
//                                  g_rx_dst_inc | UDMA_ARB_4);
//
//            uDMAChannelTransferSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT,
//                                   UDMA_MODE_BASIC,
//                                   (void *)(SSI0_BASE + SSI_O_DR),
//                                   g_ui8SSIRxBuf,
//                                   g_ui32SSIRxCount);
//
//            uDMAChannelEnable(UDMA_CHANNEL_SSI0RX);
//        }
//
//        // TX DMA complete
//        // If it's a write, g_tx_dst_inc will be UDMA_SRC_INC_8
//        // if read, g_tx_src_inc = UDMA_SRC_INC_NONE
//        if (!uDMAChannelIsEnabled(UDMA_CHANNEL_SSI0TX)) {
//            uDMAChannelControlSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
//                                  UDMA_SIZE_8 | g_tx_src_inc |
//                                  UDMA_DST_INC_NONE | UDMA_ARB_4);
//
//            uDMAChannelTransferSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT,
//                                   UDMA_MODE_BASIC,
//                                   g_ui8SSITxBuf,
//                                   (void *)(SSI0_BASE + SSI_O_DR),
//                                   g_ui32SSITxCount);
//
//            uDMAChannelEnable(UDMA_CHANNEL_SSI0TX);
//        }
//    }
//
//    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, ~GPIO_PIN_2); // indicate ISR exit
//}
