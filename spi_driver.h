#ifndef SPI_DRIVER_H_
#define SPI_DRIVER_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/udma.h"
#include "driverlib/systick.h"
#include "inc/tm4c123gh6pm.h"

//*****************************************************************************
//
// The number of SysTick ticks per second used for the SysTick interrupt.
//
//*****************************************************************************
#define SYSTICKS_PER_SECOND     100

#define DMA_MAX_TRANSFER_COUNT 1024

//// number of command address bytes
//extern volatile uint32_t g_cmd_count;

//*****************************************************************************
//
// Flags to determine the address incrementation of RX destination buffer
// and TX source buffer
//
//*****************************************************************************
extern uint32_t g_rx_dst_inc;
extern uint32_t g_tx_src_inc;

//*****************************************************************************
//
// Function prototype
//
//*****************************************************************************
void init_spi(void);
void init_udma(void);
void chip_select(uint8_t val);
void ssi_transfer(uint8_t *cmd_buf, uint32_t cmd_count, uint8_t *tx_buf,
                  uint8_t *rx_buf, uint32_t data_count);
void SSI0IntHandler(void);
void SysTickHandler(void);
void uDMAErrorHandler(void);

#endif /* SPI_DRIVER_H_ */
