# TM4C123GXL Launchpad + Flash Winbond W25Q Series
This project contains an interrupt driven DMA SPI driver which is used to interface
with the Winbond NOR flash memory. This project is developed using CCS V7

## Source Files

### spi_driver.h
Header file for spi_driver.c

### spi_driver.c
Generic SPI driver

### winbond.h
Header file for winbond.c

### winbond.c
Main program containing Winbond flash memory applications